#include <vector>
#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

Mat ROI(Mat img) {
	Mat full_img = Mat::zeros(img.cols * 2, img.rows * 4, CV_8UC3); //�.�. ����� ������� 2 ������*4 �������
	Mat rgbchannel[3];
	split(img, rgbchannel);
	Rect roi1 = Rect(0, 0, img.rows, img.cols);
	Rect roi2 = Rect(img.rows, 0, img.rows, img.cols);
	Rect roi3 = Rect(img.rows * 2, 0, img.rows, img.cols);
	Rect roi4 = Rect(img.rows * 3, 0, img.rows, img.cols);
	Rect roi5 = Rect(img.rows, img.cols, img.rows, img.cols);
	Rect roi6 = Rect(img.rows * 2, img.cols, img.rows, img.cols);
	Rect roi7 = Rect(img.rows * 3, img.cols, img.rows, img.cols);
	img.copyTo(full_img(roi1));

	Mat bchannel;
	Mat bchannel3[] = { rgbchannel[0] ,rgbchannel[0],rgbchannel[0] }; //�.�. �������� 3� ���������
	merge(bchannel3, 3, bchannel);
	bchannel.copyTo(full_img(roi2));
	
	Mat gchannel;
	Mat gchannel3[] = { rgbchannel[1] ,rgbchannel[1],rgbchannel[1] };
	merge(gchannel3, 3, gchannel);
	gchannel.copyTo(full_img(roi3));

	Mat rchannel;
	Mat rchannel3[] = { rgbchannel[2], rgbchannel[2], rgbchannel[2] }; 
	merge(rchannel3, 3, rchannel);
	rchannel.copyTo(full_img(roi4));

	Mat b;   //           b                                   g                                    r
	Mat b3[] = { rgbchannel[0], Mat::zeros(img.cols,img.rows, CV_8UC1),Mat::zeros(img.cols,img.rows, CV_8UC1) };
	merge(b3, 3, b);
	b.copyTo(full_img(roi5));
	
	Mat g;
	Mat g3[] = { Mat::zeros(img.cols,img.rows, CV_8UC1), rgbchannel[1], Mat::zeros(img.cols,img.rows, CV_8UC1) };
	merge(g3, 3, g);
	g.copyTo(full_img(roi6));

	Mat r;
	Mat r3[] = { Mat::zeros(img.cols,img.rows, CV_8UC1),Mat::zeros(img.cols,img.rows, CV_8UC1),rgbchannel[2] };
	merge(r3, 3, r);
	r.copyTo(full_img(roi7));
	

	return full_img;
}

int main() {
	Mat img = imread(".\\images\\cubeorig.jpg");
	imwrite(".\\images\\cube_65.jpg", img, { (int)IMWRITE_JPEG_QUALITY, 65 });
	imwrite(".\\images\\cube_95.jpg", img, { (int)IMWRITE_JPEG_QUALITY, 95 });
	Mat img65 = imread(".\\images\\cube_65.jpg");
	Mat img95 = imread(".\\images\\cube_95.jpg");
	Mat full_65 = ROI(img65);
	Mat full_95 = ROI(img95);


	imwrite(".\\images\\result95.jpg", full_95);
	imwrite(".\\images\\result65.jpg", full_65);
	imwrite(".\\images\\resultboth.jpg", (full_95 - full_65) * 30); //�������

	Mat img65_gray = imread(".\\images\\cube_65.jpg", IMREAD_GRAYSCALE);
	Mat img95_gray = imread(".\\images\\cube_95.jpg", IMREAD_GRAYSCALE);
	imwrite(".\\images\\resultgray.jpg", cv::abs(img65_gray - img95_gray) * 20); //�������

	waitKey(0);
	return 0;
}
