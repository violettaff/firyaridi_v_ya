#include <vector>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <fstream>
#include <iostream>

using namespace cv;
using namespace std;
const int neuralWidth = 96;
const int neuralHeight = 96;

String path = ".\\images\\";
String pictures[5] = { "01_alb_id","02_aut_drvlic_new","03_aut_id_old","04_aut_id","05_aze_passport" };
String folders[10] = { "CA","CS","HA","HS","KA","KS" };
String metki[5] = { "01","02","03","04","05" };
vector<int> keydata(1);
int points[10] = { 100, 150, 70, 230, 65, 190, 600, 160, 75, 185 }; //����� ��� ����������� ��������, ������������ � cropRotatedRect

int* readFile(int vertex[8], String path) {
	std::fstream file;
	file.open(path);

	int length = 80;
	char buffer[80];
	String s = "";

	while (!file.eof())
	{
		file.getline(buffer, length);
		for (int i = 0; i < length - 1; i++) {
			if (buffer[i] >= '0' && buffer[i] <= '9') {
				s = s + buffer[i];
			}
			if (buffer[i + 1] == ',') {
				s = s + " ";
			}
		}
	}

	std::string delimiter = " ";
	int i = 0;
	size_t pos = 0;
	std::string token;
	while ((pos = s.find(delimiter)) != std::string::npos) {
		token = s.substr(0, pos);
		vertex[i] = std::stoi(token);
		s.erase(0, pos + delimiter.length());
		i++;
	}
	vertex[7] = std::stoi(s);

	return vertex;
}

void cropRotatedRect(Mat src, RotatedRect rect, String folder, String filename, int i) {
	float angle = rect.angle;
	Size rect_size = rect.size;
	if (rect.angle < -45.) {
		angle += 90.0;
		swap(rect_size.width, rect_size.height);
	}
	Mat M, rotated, cropped;
	//rotation matrix
	M = getRotationMatrix2D(rect.center, angle, 1.0);
	
	//affine transform
	warpAffine(src, rotated, M, src.size(), INTER_CUBIC);
	
	// crop the resulting image
	getRectSubPix(rotated, rect_size, rect.center, cropped);

	//crop face
	int x = points[i*2];
	int y = points[i*2 + 1];
	double koef_y = 0.8;

	if (i == 2) {
		koef_y = 0.75;
	}

	int width = (cropped.cols - (int)(0.95 * cropped.cols));
	int height = (cropped.rows - (int)(koef_y * cropped.rows));
	Mat face;

	//make img squared
	if (width >= height) {
		Rect face_roi = Rect(x, y, width, height + (width - height));
		face = cropped(face_roi);
	}
	else {
		Rect face_roi = Rect(x, y, width + (height - width), height);
		face = cropped(face_roi);
	}

	Mat resized_face;
	resize(face, resized_face, cv::Size(neuralWidth, neuralHeight));
	imwrite(".\\results\\cropped_image\\" + folder + filename, resized_face);
}

void cropOriginalImage(Mat src, int x, int y, String filename) {
	int width = (src.cols - (int)(0.95 * src.cols));
	int height = (src.rows - (int)(0.8 * src.rows));
	Mat face;
	//make img squared
	if (width >= height) {
		Rect face_roi = Rect(x, y, width, height + (width - height));
		face = src(face_roi);
	}
	else {
		Rect face_roi = Rect(x, y, width + (height - width), height);
		face = src(face_roi);
	}
	Mat resized_face;
	resize(face, resized_face, cv::Size(neuralWidth, neuralHeight), INTER_NEAREST);
	imwrite(".\\results\\scanned_cropped_image\\"+filename, resized_face);
}

bool addGaussianNoise(const Mat src, Mat& dst, double mean = 0.0, double standartDev = 10.0)
{
	Mat src_16SC;
	Mat mGaussian_noise = Mat(src.size(), CV_16SC3);
	randn(mGaussian_noise, Scalar::all(mean), Scalar::all(standartDev));

	src.convertTo(src_16SC, CV_16SC3);
	addWeighted(src_16SC, 1.0, mGaussian_noise, 1.0, 0.0, src_16SC);
	src_16SC.convertTo(dst, src.type());

	return true;
}

void addFlashes() {
	String s = "";
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 30; j++) {
			int vertex[8];
			if (j < 9) {
				s = pictures[i] + "\\" + folders[0] + metki[i] + "_0" + std::to_string(j + 1) + ".png";
			}
			else {
				s = pictures[i] + "\\" + folders[0] + metki[i] + "_" + std::to_string(j + 1) + ".png";
			}
			Mat input_image = imread(".\\results\\cropped_image\\" + s, 1);
			Mat flash_img = input_image.clone();
			int x = rand() % 96;
			int y = rand() % 96;
			circle(flash_img, Point(x, y), 3, Scalar(255, 255, 255), -1);
			double alpha = 0.4;  //Transparency factor.
			addWeighted(flash_img, alpha, input_image, 1 - alpha, 0.0, flash_img);
			imwrite(".\\results\\cropped_image_flash\\" + s, flash_img);
		}
	}
}

void mouseCallback(int event, int x, int y, int flags, void* userdata)
{
	if (event == EVENT_LBUTTONDOWN)
	{
		Mat& img = *((Mat*)(userdata));
		circle(img, Point(x, y), 3, Scalar(0, 0, 255), 1);
		imshow("Facial keypoints", img);
		keydata.push_back(x);
		keydata.push_back(y);
	}
	if (event == EVENT_RBUTTONDOWN) {
		std::fstream file;
		file.open(".\\results\\scanned_cropped_image\\keydata\\" + pictures[0] + ".txt", ios::app);
		for (int i = 1; i < keydata.size() - 1; i++) {
			file << keydata[i] << "," << keydata[i + 1] << endl;
		}
		file.close();
	}
}

int main() {
	//<----������ � ����� images ���������� ��� �� ����������� ����� CA---->
	/*String s = "";
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 30; j++) {
			int vertex[8];
			if (j < 9) {
				s = folders[0] + metki[i] + "_0" + std::to_string(j + 1);
			}
			else {
				s = folders[0] + metki[i] +"_" + std::to_string(j + 1);
			}
			
			int* pointer = readFile(vertex, path + pictures[i] + "\\ground_truth\\" + folders[0] + "\\" + s + ".json");

			Mat img = imread(path + pictures[i] + "\\images\\" + folders[0] +"\\"+ s + ".tif");

			Point2f p1(vertex[0], vertex[1]);
			Point2f p2(vertex[2], vertex[3]);
			Point2f p3(vertex[4], vertex[5]);
			RotatedRect rect;
 			rect = RotatedRect(p1, p2, p3);
			cropRotatedRect(img, rect, pictures[i] + "\\", s + ".png", i);
		}	
	}*/
	
	//<----���������� ���� � ���������������� �����������---->
	/*for (int i = 0; i < 5; i++) {
		Mat image = imread(path + pictures[i] + "\\images\\" + pictures[i] + ".tif");
		int x = 0; int y = 0;
		std::cout << "write x, y" << endl;
		std::cin >> x >> y;
		cropOriginalImage(image, x, y, pictures[i]+".png");
	}

	//<----������������ �������� ����� �� �����������---->
	int i = 0;
	Mat image = imread(".\\results\\scanned_cropped_image\\" + pictures[i] + ".png");
	namedWindow("Facial keypoints", 1);
	setMouseCallback("Facial keypoints", mouseCallback, &image);
	imshow("Facial keypoints", image);*/

	//<----���������� ����� �� ���������� �����������---->
	/*String s = "";
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 30; j++) {
			int vertex[8];
			if (j < 9) {
				s = pictures[i] + "\\" + folders[0] + metki[i] + "_0" + std::to_string(j + 1) + ".png";
			}
			else {
				s = pictures[i] + "\\" + folders[0] + metki[i] + "_" + std::to_string(j + 1) + ".png";
			}
			Mat input_image = imread(".\\results\\cropped_image\\"  + s, 1);
			Mat mColorNoise(input_image.size(), input_image.type());
			addGaussianNoise(input_image, mColorNoise, 0, 10.0);
			imwrite(".\\results\\cropped_image_noise\\" + s, mColorNoise);
		}
	}*/

	//<----���������� ������ �� ���������� ����������� radius = 3 ---->
	//addFlashes();
	
	waitKey(0);
	return 0;
}