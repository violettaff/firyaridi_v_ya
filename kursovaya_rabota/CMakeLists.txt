cmake_minimum_required(VERSION 3.0)

project(kursovaya_rabota)

find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})

add_executable(kursovaya_rabota source.cpp)
target_link_libraries(kursovaya_rabota ${OpenCV_LIBS})